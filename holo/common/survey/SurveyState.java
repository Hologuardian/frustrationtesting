package holo.common.survey;

import holo.common.GameMain;
import holo.common.statistics.DataHandler;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.FontUtils;

public class SurveyState extends BasicGameState
{
	StateBasedGame game;
	
	int stateID;
	final int lStep = 20;
	final int qStep = 40;
	int yPos;
	TextField field;
	TextField field1;
	TextField field2;

	Image cButton;
	int cx;
	int cy;
	
	Image button;
	Image buttonA;
	boolean bSelected;
	int bx;
	int by;
	
	Image button1;
	Image button1A;
	boolean b1Selected;
	int b1x;
	int b1y;
	
	boolean b2Selected;
	int b2x;
	int b2y;
	
	boolean b3Selected;
	int b3x;
	int b3y;
	
	public static final String question = "How many hours per week do you play games?";
	public static final String question6 = "What games do you play often?";
	public static final String question1 = "On a scale of 1-10 how intense are those games?";
	public static final String question2 = "Very intense games raise your heart rate and";
	public static final String question3 = "require a lot of focus, while less intense";
	public static final String question4 = "games are more relaxed and slower pased.";
	public static final String question5 = "Do you prefer intense games?";
	
	
	public SurveyState(int id, StateBasedGame g)
	{
		stateID = id;
		game = g;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException 
	{
		yPos = 0;
		button = new Image("res/YesDeselected.png");
		buttonA = new Image("res/YesSelected.png");
		button1 = new Image("res/NoDeselected.png");
		button1A = new Image("res/NoSelected.png");
		cButton = new Image("res/Continue.png");
		
		bSelected = false;
		bx = b2x = 6;
		
		b1Selected = false;
		b1x = b3x = bx + button.getWidth() + 16;
		
		cx = gc.getWidth() - 6 - cButton.getWidth();
		cy = gc.getHeight() - 6 - cButton.getHeight();
		
		field = new TextField(gc, gc.getDefaultFont(), 6,50,500,20, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				field1.setFocus(true);
			}
		});
		field.setBorderColor(Color.gray);
		field.setBackgroundColor(Color.white);
		field.setTextColor(Color.black);
		
		field1 = new TextField(gc, gc.getDefaultFont(), 6,160,500,20, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				field2.setFocus(true);
			}
		});
		field1.setBorderColor(Color.gray);
		field1.setBackgroundColor(Color.white);
		field1.setTextColor(Color.black);

		field2 = new TextField(gc, gc.getDefaultFont(), 6,160,500,20, new ComponentListener() {
			public void componentActivated(AbstractComponent source) {
				field.setFocus(true);
			}
		});
		field2.setBorderColor(Color.gray);
		field2.setBackgroundColor(Color.white);
		field2.setTextColor(Color.black);
		
		field2.setFocus(true);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException 
	{
		yPos = 0;
		if (b3Selected)
			FontUtils.drawLeft(gc.getDefaultFont(), "Invalid field, cannot continue!", 230, 10);
		
		g.setBackground(Color.black);
		
		FontUtils.drawLeft(gc.getDefaultFont(), question6, 6, yPos += qStep);
		
		yPos += lStep;
		field2.setLocation(6, yPos);
		field2.render(gc, g);
		
		FontUtils.drawLeft(gc.getDefaultFont(), question, 6, yPos += qStep);
		field.setLocation(6, yPos += lStep);
		field.render(gc, g);
		
		FontUtils.drawLeft(gc.getDefaultFont(), question1, 6, yPos += qStep);
		FontUtils.drawLeft(gc.getDefaultFont(), question2, 6, yPos += lStep);
		FontUtils.drawLeft(gc.getDefaultFont(), question3, 6, yPos += lStep);
		FontUtils.drawLeft(gc.getDefaultFont(), question4, 6, yPos += lStep);
		field1.setLocation(6, yPos += lStep);
		field1.render(gc, g);

		FontUtils.drawLeft(gc.getDefaultFont(), question5, 6, yPos += qStep);
		
		yPos += lStep;
		by = b1y = yPos;
		
		if (!bSelected)
			button.draw(bx, yPos);
		else
			buttonA.draw(bx, yPos);
		
		if (!b1Selected)
			button1.draw(b1x, yPos);
		else
			button1A.draw(b1x, yPos);
		

		
		
		cButton.draw(cx, cy);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException 
	{
		
	}

	@Override
	public int getID() 
	{
		return stateID;
	}
	
	public void mouseClicked(int b, int x, int y, int clickCount) 
	{
		if (x > bx && x < bx + button.getWidth() &&
				y > by && y < by + button.getHeight())
		{
			bSelected = true;
			b1Selected = false;
		}
		else if(x > b1x && x < b1x + button.getWidth() &&
				y > b1y && y < b1y + button.getHeight())
		{
			b1Selected = true;
			bSelected = false;
		}
		else if(x > cx && x < cx + cButton.getWidth() &&
				y > cy && y < cy + cButton.getHeight())
		{
			beginNextStage();
		}
	}
	
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_ESCAPE)
		{
			System.exit(0);
		}
	}
	
	public void beginNextStage()
	{
		if (!field.getText().equals("") && !field1.getText().equals("") && !field2.getText().equals("") && (bSelected || b1Selected))
		{
			DataHandler.instance().addSurveyData(Integer.parseInt(field.getText()), Integer.parseInt(field1.getText()), b1Selected, field2.getText());
			game.enterState(GameMain.PLAY_STATE);
		}
		else
		{
			b3Selected = true;
		}
	}
}
