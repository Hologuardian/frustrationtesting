package holo.common;
import holo.common.statistics.DataHandler;

import javax.swing.JApplet;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;


public class Main extends JApplet
{
	public static void main(String[] args) throws SlickException 
	{
		AppGameContainer app = new AppGameContainer(new GameMain());
		  
        app.setDisplayMode(512, 400, false);
        
        if (app.isUpdatingOnlyWhenVisible())
        	app.setUpdateOnlyWhenVisible(true);
        
        app.setVSync(true);
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() { DataHandler.instance().writeData(); }
        });

        app.start();
	}
}
