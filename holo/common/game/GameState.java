package holo.common.game;

import java.util.Random;

import holo.common.statistics.DataHandler;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.FontUtils;

public class GameState extends BasicGameState
{
	Random rand = new Random();
	int stateID;
	int count;
	int timer = 1;
	GradientFill fill;
	
	Image three;
	Image two;
	Image one;
	Image zero;
	
	Shape box;
	Color col = new Color(0F, 0F, 0F);
	Shape box1;
	Color col1 = new Color(0F, 0F, 0F);
	Shape box2;
	Color col2 = new Color(0F, 0F, 0F);
	Shape box3;
	Color col3 = new Color(0F, 0F, 0F);
	
	public GameState(int id)
	{
		stateID = id;
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game) throws SlickException 
	{
		three = new Image("res/3.png");
		two = new Image("res/2.png");
		one = new Image("res/1.png");
		zero = new Image("res/0.png");
		count = 3;
		
		box = new Rectangle(0, 0, gc.getWidth() / 3, gc.getHeight() / 5);
		box.setX(10);
		box.setY(gc.getHeight() - box.getHeight() * 2);
		
		box1 = new Rectangle(0, 0, gc.getWidth() / 3, gc.getHeight() / 5);
		box1.setX(gc.getWidth() / 2 - 5 - box1.getWidth());
		box1.setY(box1.getHeight() + 40);
		
		box2 = new Rectangle(0, 0, gc.getWidth() / 3, gc.getHeight() / 5);
		box2.setX(gc.getWidth() / 2 + 5);
		box2.setY(box2.getHeight() + 40);
		
		box3 = new Rectangle(0, 0, gc.getWidth() / 3, gc.getHeight() / 5);
		box3.setX(gc.getWidth() - 10 - box3.getWidth());
		box3.setY(gc.getHeight() - box3.getHeight() * 2);
		
		updateColours();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g) throws SlickException 
	{
		FontUtils.drawCenter(gc.getDefaultFont(), "Pick a colour:", gc.getWidth() / 2, 40, 0);
		g.setBackground(Color.black);
		switch(count)
		{
		case 0:
			zero.drawCentered(gc.getWidth() / 2, gc.getHeight() - 20 - zero.getHeight());
			break;
		case 1:
			one.drawCentered(gc.getWidth() / 2, gc.getHeight() - 20 - one.getHeight());
			break;
		case 2:
			two.drawCentered(gc.getWidth() / 2, gc.getHeight() - 20 - two.getHeight());
			break;
		case 3:
			three.drawCentered(gc.getWidth() / 2, gc.getHeight() - 20 - three.getHeight());
			break;
		}
		fill = new GradientFill(box.getX(), box.getY(), col, box.getX() + box.getWidth(), box.getY() + box.getHeight(), col);
		g.draw(box, fill);
		g.fill(box, fill);
		fill = new GradientFill(box1.getX(), box1.getY(), col1, box1.getX() + box1.getWidth(), box1.getY() + box1.getHeight(), col1);
		g.draw(box1, fill);
		g.fill(box1, fill);
		fill = new GradientFill(box2.getX(), box2.getY(), col2, box2.getX() + box2.getWidth(), box2.getY() + box2.getHeight(), col2);
		g.draw(box2, fill);
		g.fill(box2, fill);
		fill = new GradientFill(box3.getX(), box3.getY(), col3, box3.getX() + box3.getWidth(), box3.getY() + box3.getHeight(), col3);
		g.draw(box3, fill);
		g.fill(box3, fill);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta) throws SlickException 
	{
		DataHandler.instance().updateTime(delta);
		
		timer += delta;
		
		if (timer >= 1000 && count > 0)
		{
			--count;
			timer = 0;
		}
		else if (count == 0)
			timer = 0;
		
	}
	
	public void updateColours()
	{
		col = new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat());
		col1 = new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat());
		col2 = new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat());
		col3 = new Color(rand.nextFloat(),rand.nextFloat(),rand.nextFloat());
	}
	
	public void mouseClicked(int b, int x, int y, int clickCount) 
	{
		DataHandler.instance().updateClicks(clickCount);
		if (count == 0)
		{
			if (box.contains(x, y))
			{
				DataHandler.instance().addColour(col);
				count = 3;
				updateColours();
			}
			else if (box1.contains(x, y))
			{
				DataHandler.instance().addColour(col1);
				count = 3;
				updateColours();
			}
			else if (box2.contains(x, y))
			{
				DataHandler.instance().addColour(col2);
				count = 3;
				updateColours();
			}
			else if (box3.contains(x, y))
			{
				DataHandler.instance().addColour(col3);
				count = 3;
				updateColours();
			}
		}
			
	}

	@Override
	public int getID() 
	{
		return stateID;
	}
	
	public void keyPressed(int key, char c) 
	{
		if (key == Input.KEY_ESCAPE)
		{
			System.exit(0);
		}
	}
}