package holo.common.statistics;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Scanner;

import org.newdawn.slick.Color;

public class DataHandler 
{
	public static DataHandler handler = new DataHandler();

	int clickCount;
	int runTime;
	Color averageColour = new Color(0.5F, 0.5F, 0.5F);
	int colours = 1;
	int sessionID;
	int intensity;
	int hoursPlayed;
	int likesIntense;

	public static DataHandler instance()
	{
		return handler;
	}


	public void addSurveyData(int hours, int intense, boolean like, String gamesPlayed)
	{
		hoursPlayed = hours;
		intensity = intense;
		//writeToFile(gamesPlayed);
		likesIntense = like ? 1 : 0;
	}

	public void addColour(Color colour)
	{
		float b = (averageColour.b * colours + colour.b);
		float r = (averageColour.r * colours + colour.r);
		float g = (averageColour.g * colours + colour.g);
		++colours;
		averageColour.b = b / colours;
		averageColour.r = r / colours;
		averageColour.g = g / colours;
	}

	public void updateClicks(int clicks)
	{
		clickCount += clicks;
	}

	public void updateTime(int time)
	{
		runTime += time;
	}

	public void writeToFile(String dataToWrite)
	{
		PrintWriter writer = null;
		Scanner sc = null;
		try
		{
			File file = new File("Games.txt");

			boolean flag;

			if (!file.exists())
			{
				file.createNewFile();
				flag = false;
			}
			else
				flag = true;

			sc = new Scanner(file);

			ArrayList<String> oldData = new ArrayList<String>();

			if (flag)
			{
				while (sc.hasNextLine())
				{
					oldData.add(sc.nextLine());
				}
			}

			writer = new PrintWriter(file);

			for (String data : oldData)
				writer.println(data);

			writer.println(dataToWrite);

			writer.close();
			sc.close();
		}
		catch (IOException eio)
		{
			eio.printStackTrace();
		}
	}

	public void writeData()
	{
		String url = "http://mark2.the-seamans.com/log/writer.php";
		String charset = "UTF-8";
		String param1 = ":" + intensity + ":" + runTime + ":" + clickCount + ":" + hashColour(averageColour) + ":" + hoursPlayed + ":" + likesIntense;
		// ...
		try 
		{
			String query = String.format(sessionID + "=%s", 
					URLEncoder.encode(param1, charset));
			URLConnection connection;
			
			connection = new URL(url).openConnection();
			
			connection.setDoOutput(true); // Triggers POST.
			connection.setRequestProperty("Accept-Charset", charset);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
			OutputStream output = null;
			
			
			try 
			{
				output = connection.getOutputStream();
				output.write(query.getBytes(charset));
			} 
			finally 
			{
				if (output != null) try { output.close(); } catch (IOException logOrIgnore) {}
			}
			
			@SuppressWarnings("unused")
			InputStream response = connection.getInputStream();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public int hashColour(Color colour)
	{
		return (int) (((int)(colour.r * 255) << 16) + ((int)(colour.g * 255) << 8) + colour.b * 255);
	}
}
