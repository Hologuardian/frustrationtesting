package holo.common;

import holo.common.game.GameState;
import holo.common.survey.SurveyState;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class GameMain extends StateBasedGame
{
	public static final int QUESTION_STATE = 0;
	public static final int PLAY_STATE = 1;
	
	
	public GameMain() {
		super("Survey");
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException 
	{
		this.addState(new SurveyState(QUESTION_STATE, this));
		this.addState(new GameState(PLAY_STATE));
	}

}
